package game;

public class Hero implements Character {
    private int strength;

    public Hero(int strength) {
        this.strength = strength;
    }

    @Override
    public int getPower() {
        return this.strength;
    }
}
