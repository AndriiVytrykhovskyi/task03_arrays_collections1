package game;

public class MagicArtifact implements Character {
    private int superPower;

    public MagicArtifact(int superPower) {
        this.superPower = superPower;
    }

    @Override
    public int getPower() {
        return this.superPower;
    }

    @Override
    public String toString() {
        return "Artifact ";
    }
}
