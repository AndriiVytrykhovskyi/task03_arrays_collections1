package game;

public class GameMain {
    public static void main(String[] args) {
        Room room = new Room(10);
        room.printInfo();
        System.out.println("Hero`s dead are behind "
                + room.countDoorsWithMonsters(0, 0) + " doors.");
        for (int i : room.getWayToWin()) {
            System.out.print((i + 1) + " ");
        }
    }
}
