package game;

public class Monster implements Character {

    private int attack;

    public Monster(int attack) {
        this.attack = attack;
    }

    @Override
    public int getPower() {
        return this.attack;
    }

    @Override
    public String toString() {
        return "Monster ";
    }
}
