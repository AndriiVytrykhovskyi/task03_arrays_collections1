package game;

import array.Array;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Room {
    private Character[] doors;
    private Hero hero;
    private Monster monster;

    public Room(int n) {
        this.doors = new Character[n];
        this.hero = new Hero(25);
        generateCharactersBehindTheDoor();
    }

    public void generateCharactersBehindTheDoor() {
        for (int i = 0; i < doors.length; i++) {
            int generate = ThreadLocalRandom.current().nextInt(0, 11);
            if (generate % 2 == 0) {
                doors[i] = new Monster(ThreadLocalRandom.current().nextInt(5, 101));
            } else {
                doors[i] = new MagicArtifact(ThreadLocalRandom.current().nextInt(10, 81));
            }
        }
    }

    public int generateNumberOfDoor() {
        return ThreadLocalRandom.current().nextInt(0, doors.length);
    }

    public void printInfo() {
        int i = 0;
        while (i < doors.length) {
            System.out.println((i + 1) + " Door " + doors[i].getClass().getSimpleName()
                    + " " + doors[i].getPower());
            i++;
        }
    }

    public int countDoorsWithMonsters(int index, int monsterDoors) {
        if (index == doors.length) {
            return monsterDoors;
        }
        if (doors[index].getClass().getSimpleName().equals("Monster")
                && hero.getPower() < doors[index].getPower()) {
            monsterDoors++;
        }
        return countDoorsWithMonsters(index + 1, monsterDoors);
    }

    public boolean isHeroWinner() {
        int heroHealth = hero.getPower();
        int monsterHealth = 0;
        boolean isHeroWinner = true;
        for (Character door : doors) {
            if (door instanceof Monster) {
                monsterHealth += door.getPower();
            } else {
                heroHealth += door.getPower();
            }
        }
        if (monsterHealth > heroHealth) {
            System.out.println("There is no way to stay alive:(");
            isHeroWinner = false;
        }
        return isHeroWinner;
    }


    public int[] getWayToWin() {
        if (!isHeroWinner()) {
            System.exit(0);
        }
        int heroHealth;
        int way[] = new int[doors.length];
        while (true) {
            Arrays.fill(way, -1);
            heroHealth = hero.getPower();
            for (int i = 0; i < doors.length; i++) {
                int index = generateNumberOfDoor();
                while (!Array.isUnique(Arrays.copyOfRange(way, 0, i), index)) {
                    index = generateNumberOfDoor();
                }
                way[i] = index;

                if (doors[index] instanceof Monster) {
                    heroHealth -= doors[index].getPower();
                } else {
                    heroHealth += doors[index].getPower();
                }
                if (heroHealth < 0) {
                    break;
                }
            }
            if (heroHealth >= 0) {
                break;
            }
        }
        System.out.print("Way: ");
        return way;
    }

    public Character getDoors(int index) {
        return doors[index];
    }

}
