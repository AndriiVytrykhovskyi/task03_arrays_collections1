package array;

public class Program {
    public static void main(String[] args) {
        Array array = new Array();

        System.out.println("Arrays:");
        array.printArrays();

        System.out.println("Joint array:");
        array.createJointArray();

        System.out.println("Unique first array:");
        array.createUniqueArrays(array.getArrayOne(), array.getArrayTwo());
        System.out.println("Unique second array:");
        array.createUniqueArrays(array.getArrayTwo(), array.getArrayOne());

        System.out.println("First array without repeats:");
        array.deleteElements(array.getArrayOne());
        System.out.println("Second array without repeats:");
        array.deleteElements(array.getArrayTwo());

        System.out.println("Delete the same numbers in raw in first array:");
        array.deleteNumbersInARow(array.getArrayOne());
        System.out.println("Delete the same numbers in raw in second array:");
        array.deleteNumbersInARow(array.getArrayTwo());

    }
}

