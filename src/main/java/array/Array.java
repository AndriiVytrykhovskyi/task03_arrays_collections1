package array;

import java.util.Arrays;
import java.util.Scanner;

public class Array {
    private int arrayOne[];
    private int arrayTwo[];

    public Array() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Set size of first array:");
        int sizeOne = sc.nextInt();
        System.out.println("Set size of second array:");
        int sizeTwo = sc.nextInt();

        arrayOne = new int[sizeOne];
        arrayTwo = new int[sizeTwo];

        System.out.println("Set numbers to first:");
        for (int i = 0; i < sizeOne; i++) {
            arrayOne[i] = Integer.parseInt(sc.next());
        }
        System.out.println("Set numbers to second:");
        for (int i = 0; i < sizeTwo; i++) {
            arrayTwo[i] = Integer.parseInt(sc.next());
        }
    }

    public int[] getArrayOne() {
        return arrayOne;
    }

    public int[] getArrayTwo() {
        return arrayTwo;
    }

    public void printArrays() {
        printArray(arrayOne);
        printArray(arrayTwo);
    }

    public void printArrayToSize(int size, int arr[]) {
        if (size == 0) {
            System.out.println("Array is empty");
        } else {
            for (int i = 0; i < size; i++) {
                System.out.print(arr[i] + " ");
            }
        }
        System.out.println();
    }

    public void printArray(int[] arr) {
        printArrayToSize(arr.length, arr);
        System.out.println();
    }

    public void createUniqueArrays(int arrOne[], int arrTwo[]) {
        int sizeOfThirdMassive = Math.max(arrayOne.length, arrayTwo.length);
        int arrayThree[] = new int[sizeOfThirdMassive];
        int index = 0;
        for (int anArrOne : arrOne) {
            if (isUnique(arrTwo, anArrOne)) {
                if (index == 0) {
                    arrayThree[index++] = anArrOne;
                } else if (isUnique(Arrays.copyOfRange(arrayThree, 0, index), anArrOne)) {
                    arrayThree[index++] = anArrOne;
                }
            }
        }
        printArrayToSize(index, arrayThree);
    }

    public void createJointArray() {
        int sizeOfThirdMassive = Math.min(arrayOne.length, arrayTwo.length);
        int arrayThree[] = new int[sizeOfThirdMassive];
        int index = 0;

        for (int anArrayOne : arrayOne) {
            if (!isUnique(arrayTwo, anArrayOne)) {
                if (index == 0) {
                    arrayThree[index++] = anArrayOne;
                } else if (isUnique(Arrays.copyOfRange(arrayThree, 0, index), anArrayOne)) {
                    arrayThree[index++] = anArrayOne;
                }
            }
        }
        printArrayToSize(index, arrayThree);
    }

    public static boolean isUnique(int[] array, int element) {
        for (int anArray : array) {
            if (element == anArray) {
                return false;
            }
        }
        return true;
    }

    public void deleteElements(int[] arr) {
        int n = arr.length;
        int[] array = new int[n];
        System.arraycopy(arr, 0, array, 0, n);
        int counter = 0;

        for (int i = 0; i < n; i++) {
            counter = 1;
            for (int j = i + 1; j < n; j++) {
                if (array[j] == array[i]) {
                    counter++;
                }
            }
            if (counter > 1) {
                int one = array[i];
                for (int j = i; j < n; j++) {
                    if (array[j] == one) {
                        System.arraycopy(array, j + 1, array, j, n - (j + 1));
                        n--;
                        j--;
                    }
                }
                i--;
            }
        }
        printArrayToSize(n, array);
    }

    public void deleteNumbersInARow(int[] arr) {
        int n = arr.length;
        int counter = 1;
        int i = 1;

        while (i != n) {
            if (arr[i - 1] == arr[i]) {
                counter++;
            } else if (counter > 1) {

                System.arraycopy(arr, i - 1, arr, i - counter, n - i + 1);
                n = n - counter + 1;
                i = i - counter + 1;
                counter = 1;
            }
            i++;
        }
        if (counter > 1) {
            printArrayToSize(n - counter + 1, arr);
        } else {
            printArrayToSize(n, arr);
        }
    }
}
