package collection.customstring;

public class StringContainer {
    private String[] container;
    private int size;
    private static final int MAX_SIZE = Integer.MAX_VALUE - 5;

    public StringContainer() {
        this.container = new String[10];
        this.size = 0;
    }

    public boolean add(String s) {
        if (size == MAX_SIZE) {
            return false;
        }
        if (size == container.length) {
            int newContainerSize = (container.length * 3) / 2 + 1;
            if (newContainerSize > MAX_SIZE){
                newContainerSize = MAX_SIZE;
            } else{
                newContainerSize = (container.length * 3) / 2 + 1;
            }
            String [] newContainer = new String[newContainerSize];
            System.arraycopy(container,0,newContainer,0,container.length);
            container = newContainer;
        }
        container[size++]=s;
        return true;
    }

    public String getContainer(int index) {
        return container[index];
    }

    public int getSize() {
        return size;
    }
}
