package collection.customstring;

import java.util.ArrayList;
import java.util.List;

public class StringProgram {
    public static void main(String[] args) {
        StringContainer stringContainer = new StringContainer();
        System.out.println("Custom String Container");
        long start = System.currentTimeMillis();
        for (int i = 0; i < 3000000; i++) { //5936 ms
            String s = String.valueOf(i);
            stringContainer.add(s);
        }
        long end = System.currentTimeMillis();
        System.out.println((end - start) + " ms"); //8852 ms
        System.out.println("Size = " + stringContainer.getSize());
        System.out.println("100 element = " + stringContainer.getContainer(100));

        List<String> list = new ArrayList<>();
        System.out.println("\nArrayList<String>:");
        start = System.currentTimeMillis();
        for (int i = 0; i < 3000000; i++) { //1914 ms
            String s = String.valueOf(i);
            list.add(s);
        }
        end = System.currentTimeMillis();
        System.out.println((end - start) + " ms");
        System.out.println("Size = " + list.size());
        System.out.println("100 element = " + list.get(100));
    }
}
