package collection.countries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CountryProgram {
    public static void main(String[] args) {
        List<Country> list = new ArrayList<>();

        Country countryToFind = new Country("Russia", "Moscow");
        Country anotherCountryToFind = new Country("Spain", "Barcelona");

        Comparator<Country> comparator = Comparator.comparing(Country::getCapital);

        list.add(new Country("Ukraine", "Kyiv"));
        list.add(new Country("Germany", "Berlin"));
        list.add(new Country("Norway", "Oslo"));
        list.add(new Country("Netherlands", "Amsterdam"));
        list.add(new Country("France", "Paris"));
        list.add(new Country("Spain", "Barcelona"));

        for (Country country : list) {
            System.out.println(country);
        }

        System.out.println("\nSorted by name from A to Z");
        list.sort((o1, o2) -> o1.compareTo(o2)); //A-Z uses lambda
        for (Country country : list) {
            System.out.println(country);
        }

        System.out.println("\nSorted by name from Z to A");
        Collections.reverse(list);//Z-A
        for (Country country : list) {
            System.out.println(country);
        }

        System.out.println("\nSorted by capital:");
        list.sort(comparator);
        for (Country country : list) {
            System.out.println(country);
        }

        System.out.println("\nBinary Search of " + countryToFind + " and " + anotherCountryToFind);

        int indexOne = Collections.binarySearch(list, countryToFind, comparator);
        int indexTwo = Collections.binarySearch(list, anotherCountryToFind, comparator);
        System.out.println(countryToFind + " at position " + indexOne);
        System.out.println(anotherCountryToFind + " at position " + indexTwo);
    }
}
