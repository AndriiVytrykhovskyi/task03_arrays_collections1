package generics;

import java.util.ArrayList;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        //  Ship<String> ship = new Ship<>(); //error
        Ship<Droid> shipOfDroid = new Ship<>();
        Droid k2s0 = new K2SO("K2SO", 100, 2, 50);
        Droid ultron = new K2SO("Ultron", 1000, 100, 500);
        shipOfDroid.add(k2s0);
        shipOfDroid.add(ultron);
        // shipOfDroid.add("droid"); //error
        for (Droid aShipOfDroid : shipOfDroid) {
            System.out.println(aShipOfDroid);
        }

        K2SO droidOne = new K2SO("Droid One", 300, 10, 30);
        Ship<K2SO> shipOfK2SO = new Ship<>();
        for (K2SO aShipOfK2SO : shipOfK2SO) {
            System.out.println(aShipOfK2SO);
        }

        PriorityQueue<Droid> priorityQueue = new PriorityQueue<>();
        priorityQueue.addAll(shipOfDroid);
        while (!priorityQueue.isEmpty()){
            System.out.println(priorityQueue.poll().getName());
        }
    }
}
